/**
 * @file This is the settings of the default theme
 * @since 1.0.0
 * @author Tristan Chatman
 */

// Imports
import templates from './templates'
import { Theme } from 'kaiba'

// Exports
export default new Theme({
  name: 'Default Theme',
  slug: 'default-theme',
  templates: templates
})