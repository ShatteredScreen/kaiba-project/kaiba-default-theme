/**
 * @file This file will be used to import all the templates from the theme
 * @since 1.0.0
 * @author Tristan Chatman
 */

// Init the context and storage
let templates = new Map()
let req = require.context('./', true, /.vue$/)
for (let key of req.keys()) {
  templates.set(key, req(key).default)
}

// Exports
export default templates
